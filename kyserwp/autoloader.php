<?php

function load_settings() // Settings :: Hook = setup_theme
{
    foreach(glob(get_stylesheet_directory() . "/settings/*.php") as $setting){
        require $setting;
    }
}

add_action('setup_theme', 'load_settings');

    function load_classes() // Classes :: Hook = init
    {
        foreach(glob(get_stylesheet_directory() . "/classes/*.php") as $class){
            require $class;
        }
    }

    add_action('init', 'load_classes');




// PHP Functions autoload php files in the tasks directory || this is to clean up and organize theme functions by allowing them to easily be different files
    function load_tasks() // Tasks :: Hook = wp_loaded
    {
        foreach(glob(get_stylesheet_directory() . "/tasks/*.php") as $task){
            require $task;
        }
    }

    add_action('wp_loaded', 'load_tasks');

    function load_shortcodes() // Shortcodes :: Hook = wp_head
    {
        foreach(glob(get_stylesheet_directory() . "/shortcodes/*.php") as $shortcode){
            require $shortcode;
        }
    }

    add_action('wp_head', 'load_shortcodes');



function load_options() // Option Pages :: Hook = wp_loaded

{
    foreach(glob(get_stylesheet_directory() . "/options/*.php") as $option){
        require $option;
    }
}

add_action('wp_loaded', 'load_options');



    function load_extras(){
        require_once get_stylesheet_directory() . '/sidebars.php'; // Sidebars
        require_once get_stylesheet_directory() . '/widgets.php'; // Widgets

    }

    add_action('wp_head', 'load_extras');

    // Javascript and CSS file autoloader (replace stylesheet_directory with template_directory if this not a child theme) :: Hook = wp_loaded

    function enqueue_theme_assets()
    {

        //Third Party first

        wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/assets/thirdparty/bootstrap/js/bootstrap.bundle.min.js');
        wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/assets/thirdparty/bootstrap/css/bootstrap.min.css');



        $dirJS = new DirectoryIterator(get_stylesheet_directory() . '/js');
        $dirCSS = new DirectoryIterator(get_stylesheet_directory() . '/css');
        //


        foreach ($dirJS as $file) {

            if (pathinfo($file, PATHINFO_EXTENSION) === 'js') {
                $name = basename($file, '.js');
                $name_and_ext = basename($file);


                wp_enqueue_script($name, get_stylesheet_directory_uri() . '/js/' . $name_and_ext);

            }

        }

        $conditionalCSS = array(
           // 'homepage' => is_page('front'),
        );

        foreach ($dirCSS as $style) {

            if (pathinfo($style, PATHINFO_EXTENSION) === 'css' && !in_array(basename($style), array_keys($conditionalCSS))) {
                $s_name = basename($style, '.css');
                $s_name_and_ext = basename($style);


                wp_enqueue_style($s_name, get_stylesheet_directory_uri() . '/css/' . $s_name_and_ext);

            }

        }

        array_walk($conditionalCSS, function (&$el, &$key) {
            if($el) {
                wp_enqueue_style($key, get_stylesheet_directory_uri() . '/css/' . $key);
            }
        });



    }

    add_action('wp_enqueue_scripts', 'enqueue_theme_assets');





