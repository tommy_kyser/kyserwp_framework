<?php

namespace Kyser;


class super_css
{
    public function _new($styles){
        echo '<style>';
        foreach ($styles as $class => $props){
            if($props !== '') {
                echo $class; ?>
                {
                <?php foreach ($props as $prop => $value)
                {
                    echo $prop . $value . ';';
                }
                ?>}

            <?php }

        }
        echo '</style>';
    }

    public function _new_file($file_name, $dir, $styles){
        ob_start();
        foreach ($styles as $class => $props){
            if($props !== '') {
                echo $class; ?>
                {
                <?php foreach ($props as $prop => $value)
                {
                    echo $prop . $value . ';';
                }
                ?>}

            <?php }

        }
        $data = ob_get_contents();
        ob_end_clean();
        file_put_contents(get_stylesheet_directory() . $dir . $file_name , $data);
    }

}