<?php
$kyser_options = new \Kyser\super_options();

$kyser_options->_addPage('KyserWP', 'options');
$kyser_options->_newGroup('general-options','General', 'options');
$kyser_options->_addField('general-options', 'site_title', 'Site Title', 'text');