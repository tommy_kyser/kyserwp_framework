<?php ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php wp_head(); ?>
</head>

<body>
<div id="header_wrap" class="site">
    <header>
        <div class="site-branding-container">

        </div>
            <div class="site-featured-image">

            </div>

    </header><!-- #masthead -->
</div>
    <div id="content" class="site-content">
