Requirements:

    ACF PRO
    Gravity Forms
    Gravity View
    Classic Editor
    

Install:

    Either upload the parent folder into your themes dir or zip the parent and install as normal theme.
    
Config:

Read through the autoloader.php and set vars as needed


Each auto load dir is set to fire on different wp hooks

settings -> setup_theme

classes -> init

tasks -> wp_loaded

shortcodes -> wp_head

options -> wp_loaded

enqueue_theme_assets -> enqueue_scripts




Features:

-ACF Pro Extentions

super_options class

This class has methods to create an admin options page, sub pages,field groups, and fields.




Notes: 

Theme includes Bootstrap 4 natively and my work in progress super_css class for dynamic php powered css 